package com.devcamp.s10jacksonjson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S10JacksonJsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(S10JacksonJsonApplication.class, args);
	}

}
